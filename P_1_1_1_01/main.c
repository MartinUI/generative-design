#include <stdio.h>
#include "raylib.h"
#include "raymath.h"

void TakeTimeStampedScreenshot();

int main(int argc, char *argv[])
{
	// Initialization
	//--------------------------------------------------------------------------------------
	int screen_width = 800;
	int screen_height = 400;

	int step_x = 0;
	int step_y = 0;

	Vector3 hsv_rect_color;
	Color rect_color;

	InitWindow(screen_width, screen_height, "P_1_1_1_01");

	SetTargetFPS(60);
	// Main game loop
	while (!WindowShouldClose()) // Detect window close button or ESC key
	{
		// Update
		if (IsKeyDown(KEY_S)) TakeTimeStampedScreenshot();

		int mouse_x = Clamp(GetMouseX(), 0, screen_width);
		int mouse_y = Clamp(GetMouseY(), 0, screen_height);
		// Draw
		BeginDrawing();

		ClearBackground(RAYWHITE);

		step_x = mouse_x + 2;
		step_y = mouse_y + 2;		

		for (int grid_y = 0; grid_y < screen_height; grid_y += step_y)
		{
			for (int grid_x = 0; grid_x < screen_width; grid_x += step_x)
			{
				hsv_rect_color.x = Lerp(0.0, 360.0, (float)grid_x / (float)screen_width);
				hsv_rect_color.y = Lerp(0.0, 1.0, 1.0 - (float)grid_y / (float)screen_height);
				hsv_rect_color.z = 1.0;
				rect_color = ColorFromHSV(hsv_rect_color);
				DrawRectangle(grid_x, grid_y, step_x, step_y, rect_color);
			}
		}

		EndDrawing();
	}

	// De-Initialization
	CloseWindow(); // Close window and OpenGL context
	return 0;
}

void TakeTimeStampedScreenshot()
{
	//Create file name buffer
	char curent_time[200];
	//Pull time stamp
	double time_stamp = GetTime();
	//Coppy time stamp into character buffer with one milisecond precision
	sprintf(curent_time, "%.3lf.png", time_stamp);
	//Take screenshot
	TakeScreenshot((const char *)curent_time);
}


