#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include "raylib.h"
//#include "stretchy_buffer.h"

Color HSVToColor(Vector3, int);
void TakeTimeStampedScreenshot();
int map(int, int, int, int, int);
int clamp(int, int);
int main(int argc, char *argv[])
{
    // Initialization
    int screen_width = 800;
    int screen_height = 800;

    int tiel_count_x = 50;
    int tiel_count_y = 10;

    int color_count = 20;

    float hue_values[color_count];
    float saturation_values[color_count];
    float brightness_values[color_count];

    InitWindow(screen_width, screen_height, "P_1_2_3_02");

    //SetTargetFPS(60);

    for (int i = 0; i < color_count; i++)
    {
        if (i % 2 == 0)
        {
            hue_values[i] = (float)GetRandomValue(0, 360);
            saturation_values[i] = 1.0;
            brightness_values[i] = (float)GetRandomValue(10, 100) / 100;
        }
        else
        {
            hue_values[i] = 195.0;
            saturation_values[i] = (float)GetRandomValue(0, 100) / 100;
            brightness_values[i] = 1.0;
        }
    }
    
    

    // Main game loop


    bool should_close = false;

    //while (!WindowShouldClose()) // Detect window close button or ESC key
    while(!should_close)
    {
        //oryginal book example 0-9 replacement withQ-P
        if (IsKeyDown(KEY_S))
            TakeTimeStampedScreenshot();
        else if (IsKeyReleased(KEY_Q))
        {
            for (int i = 0; i < color_count; i++)
            {
                hue_values[i] = (float)GetRandomValue(0, 360);
                saturation_values[i] = (float)GetRandomValue(0, 100) / 100;
                brightness_values[i] = (float)GetRandomValue(0, 100) / 100;
            }
        }

    // count every tile
    int counter = 0;

    int row_count = GetRandomValue(5, 40);
    printf("Row count %d",row_count);


    float row_height = (float)screen_height / (float)row_count;

    float parts[2048];
    int part_slot = 0;
    float part;


    BeginDrawing();

        for (int j = 0; j < part_count; j++)
        {
            float rand_value = (float)GetRandomValue(0, 100) / 100;

            if (rand_value < 0.075)
            {
                // take care of big values
                int fragments = GetRandomValue(2, 20);

                part_count = part_count + fragments;
                for (int k = 0; k < fragments; k++)
                {
                    parts[part_slot] = (float)GetRandomValue(0, 200) / 100;
                    part_slot++;
                }
            }
            else
            {
                parts[part_slot] = (float)GetRandomValue(20, 200) / 100;
                part_slot++;
            }
        }

        

        // add all subparts
        float sum_parts_total = 0;

        for (int j = 0; j < part_count; j++)
            sum_parts_total += parts[j];
        // Draw
        printf("Sum parts total %f", sum_parts_total);

        float sum_parts_now = 0;
        int count = sb_count(parts);
        for (int x = 0; x < count; x++)
        {
            // get component color values
            int index = counter % color_count;

            Vector3 hsv_fill = {hue_values[index], saturation_values[index], brightness_values[index]};
            Color fill = HSVToColor(hsv_fill, 255);

            sum_parts_now += parts[x];

            DrawRectangle(map(sum_parts_now, 0, sum_parts_total, 0, screen_width), row_height * i,
                          map(parts[x], 0, sum_parts_total, 0, screen_width) * -1, row_height, fill);

            counter++;
        }
    }
    //sb_free(parts);        
    EndDrawing();
    }
    getchar();
    should_close = true;

    // De-Initialization
    CloseWindow(); // Close window and OpenGL context
    return 0;
}

//Hue 0.0-360.0 float Saturation 0.0-1.0, Level 0.0-1.0, alpha 0-255

Color HSVToColor(Vector3 hsv, int alpha)
{
    float r, g, b;
    r = g = b = 0.0;

    float h = hsv.x / 360;
    float s = hsv.y;
    float v = hsv.z;

    int i = floor(h * 6);
    float f = h * 6 - i;
    float p = v * (1 - s);
    float q = v * (1 - f * s);
    float t = v * (1 - (1 - f) * s);

    switch (i % 6)
    {
    case 0:
        r = v, g = t, b = p;
        break;
    case 1:
        r = q, g = v, b = p;
        break;
    case 2:
        r = p, g = v, b = t;
        break;
    case 3:
        r = p, g = q, b = v;
        break;
    case 4:
        r = t, g = p, b = v;
        break;
    case 5:
        r = v, g = p, b = q;
        break;
        //Just in case
    case 6:
        r = v, g = t, b = p;
        break;
    case -1:
        r = v, g = p, b = q;
        break;
    }

    Color rgba = {0, 0, 0, 0};

    rgba.r = r * 255;
    rgba.g = g * 255;
    rgba.b = b * 255;
    rgba.a = alpha;

    return rgba;
}

void TakeTimeStampedScreenshot()
{
    //Create file name buffer
    char curent_time[200];
    //Pull time stamp
    double time_stamp = GetTime();
    //Coppy time stamp into character buffer with one milisecond precision
    sprintf(curent_time, "%.3lf.png", time_stamp);
    //Take screenshot
    TakeScreenshot((const char *)curent_time);
}
//simple int value mapper

int map(int x, int in_min, int in_max, int out_min, int out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
//simple int clamp to l limit

int clamp(int i, int l)
{
    if (i < 0)
        return 0;
    if (i > l)
        return l;
    return i;
}
