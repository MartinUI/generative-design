#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include "raylib.h"
#include "raymath.h"

Color HSVToColor(Vector3, int);
void TakeTimeStampedScreenshot();

int main(int argc, char *argv[])
{
	// Initialization
	//--------------------------------------------------------------------------------------
	int screen_width = 800;
	int screen_height = 800;

	const float to_rad = PI / 180.0;

	int angle, angle_step;
	int segments = 24;

	int mouse_x, mouse_y;

	int radius, center_x, center_y;
	
	Vector3 hsv_color;
	Vector2 t1, t2, t3;
	Color color;

	InitWindow(screen_width, screen_height, "P_1_1_2_01");

	SetTargetFPS(60);

	// Main game loop
	while (!WindowShouldClose()) // Detect window close button or ESC key
	{
		// Update
		if (IsKeyDown(KEY_S)) {
			TakeTimeStampedScreenshot();
		} else if (IsKeyDown(KEY_Q)) {
			segments = 360;
		} else if (IsKeyDown(KEY_W)) {		
			segments = 45;
		} else if (IsKeyDown(KEY_E)) {		
			segments = 24;
		} else if (IsKeyDown(KEY_T)) {		
			segments = 12;
		} else if (IsKeyDown(KEY_Y)) {		
			segments = 6;
		}		
		
		mouse_x = Clamp(GetMouseX(), 0, screen_width);
		mouse_y = Clamp(GetMouseY(), 0, screen_height);
		// Draw
		BeginDrawing();

		ClearBackground(RAYWHITE);

		hsv_color.y = 1.0 / screen_width * mouse_x;
		hsv_color.z = 1.0 / screen_height * mouse_y;

		angle_step = 360 / segments;
		radius = screen_width / 2 - 25;

		center_x = screen_width / 2;
		center_y = screen_height / 2;

		t1.x = center_x;
		t1.y = center_y;
		
		for (angle = 0; angle <= 360; angle += angle_step)
		{
			//Secont trialange point
			t2.x = center_x + cos(angle * to_rad) * radius;
			t2.y = center_x + sin(angle * to_rad) * radius;
			t3.x = center_x + cos((angle + angle_step) * to_rad) * radius;
			t3.y = center_x + sin((angle + angle_step) * to_rad) * radius;
			// Hue value
			hsv_color.x = 360.0 / 360 * angle;			
			//color = HSVToColor(hsv_color, 255);
			color = ColorFromHSV(hsv_color);
			// Order of the  vertecies is important!
			DrawTriangle(t2,t1,t3,color);		
		}
		EndDrawing();
	}
	// De-Initialization
	CloseWindow(); // Close window and OpenGL context
	return 0;
}

void TakeTimeStampedScreenshot()
{
	//Create file name buffer
	char curent_time[200];
	//Pull time stamp
	double time_stamp = GetTime();
	//Coppy time stamp into character buffer with one milisecond precision
	sprintf(curent_time, "%.3lf.png", time_stamp);
	//Take screenshot
	TakeScreenshot((const char *)curent_time);
}

