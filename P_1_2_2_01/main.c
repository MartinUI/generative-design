#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#include "raylib.h"

enum sort_mode {
    NONE, HUE, SATURATION, BRIGHTNES
};

Color HSVToColor(Vector3, int);
void TakeTimeStampedScreenshot();
void ColorPicker(Image, Color *, Color *, int, int);
void HSLColorsSort(Vector3 [], int, enum sort_mode);

int max(int, int);
void swap_vectors3 (Vector3 *, Vector3 *);
int hsl_hue_compare(const void *, const void *);

int main() {
    // Initialization
    //--------------------------------------------------------------------------------------
    int screen_width = 600;
    int screen_height = 600;

    InitWindow(screen_width, screen_height, "P_1_2_2_01");

    // NOTE: Textures MUST be loaded after Window initialization (OpenGL context is required)
    Image image1 = LoadImage("data/pic1.png"); // Loaded in CPU memory (RAM)
    Texture2D texture1 = LoadTextureFromImage(image1); // Image converted to texture, GPU memory (VRAM)
    Color *image_buf1 = GetImageData(image1);
    

    Image image2 = LoadImage("data/pic2.png");
    Texture2D texture2 = LoadTextureFromImage(image2);
    Color *image_buf2 = GetImageData(image2);

    Image image3 = LoadImage("data/pic3.png");
    Texture2D texture3 = LoadTextureFromImage(image3);
    Color *image_buf3 = GetImageData(image3);

    int picked_image = 0;
    enum sort_mode mode = NONE;


    UnloadImage(image1);
    UnloadImage(image2);
    UnloadImage(image3);
    
    // Main game loop	
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        // Update
        if (IsKeyDown(KEY_S)) {
            TakeTimeStampedScreenshot();
        } else if (IsKeyDown(KEY_Q)) {
            picked_image = 0;
        } else if (IsKeyDown(KEY_W)) {
            picked_image = 1;
        } else if (IsKeyDown(KEY_E)) {
            picked_image = 2;
        } else if (IsKeyPressed(KEY_R)) {
            mode = NONE;
        } else if (IsKeyPressed(KEY_T)) {
            mode = HUE;
        } else if (IsKeyPressed(KEY_Y)) {
            mode = SATURATION;
        } else if (IsKeyPressed(KEY_U)) {
            mode = BRIGHTNES;
        }
        

        BeginDrawing();

        ClearBackground(RAYWHITE);

        int tileCount = screen_width / max(GetMouseX(), 5);
        float rectSize = screen_width / (float) tileCount;

        // get colors from image
        int i = 0;
        int n = 0;
        int t = tileCount * tileCount;

        Color colors[t];
        Vector3 hsl_colors[t];

        for (int gridY = 0; gridY < tileCount; gridY++) {
            for (int gridX = 0; gridX < tileCount; gridX++) {
                int px = (int) (gridX * rectSize);
                int py = (int) (gridY * rectSize);

                switch (picked_image) {
                    case 0:
                        ColorPicker(image1, image_buf1, &colors[i], px, py);
                        break;
                    case 1:
                        ColorPicker(image2, image_buf2, &colors[i], px, py);
                        break;
                    case 2:
                        ColorPicker(image3, image_buf3, &colors[i], px, py);
                        break;
                }

                i++;
            }
        }

        i = 0;  

        //Create HSV structs array
        for (n = 0; n < t; n++) { 
            hsl_colors[n] = ColorToHSV(colors[n]);
        }        
       
        switch(mode){
            case NONE:
                break;
            case HUE:{
                //Standard qsort
                qsort(hsl_colors,t,sizeof(Vector3),hsl_hue_compare);
                //Handmade bouble sort
                //HSLColorsSort(hsl_colors,t, HUE);                
                for (int c = 0; c < t; c++) {
                    colors[c] = HSVToColor(hsl_colors[c], 255);                
                }
                break;
            }
            case SATURATION:{
                HSLColorsSort(hsl_colors,t, SATURATION);                
                for (int c = 0; c < t; c++) {
                    colors[c] = HSVToColor(hsl_colors[c], 255);                
                }
                break;
            }
            case BRIGHTNES:{
                HSLColorsSort(hsl_colors,t, BRIGHTNES);                
                for (int c = 0; c < t; c++) {
                    colors[c] = HSVToColor(hsl_colors[c], 255);                
                }
                break;
            }
        }        
        //Draw color grid
        for (int gridY = 0; gridY < tileCount; gridY++) {
            for (int gridX = 0; gridX < tileCount; gridX++) {
                DrawRectangle(gridX * rectSize, gridY * rectSize, rectSize + 1, rectSize + 1, colors[i]);
                i++;
            }
        }
        EndDrawing();
    }

    // De-Initialization
    UnloadTexture(texture1); // Texture unloading
    UnloadTexture(texture2);
    UnloadTexture(texture3);

    CloseWindow(); 
    return 0;
}

//Hue 0.0-360.0 float Saturation 0.0-1.0, Level 0.0-1.0, alpha 0-255
Color HSVToColor(Vector3 hsv, int alpha) {
    float r, g, b;
    r = g = b = 0.0;

    float h = hsv.x / 360;
    float s = hsv.y;
    float v = hsv.z;

    int i = floor(h * 6);
    float f = h * 6 - i;
    float p = v * (1 - s);
    float q = v * (1 - f * s);
    float t = v * (1 - (1 - f) * s);

    switch (i % 6) {
        case 0:
            r = v, g = t, b = p;
            break;
        case 1:
            r = q, g = v, b = p;
            break;
        case 2:
            r = p, g = v, b = t;
            break;
        case 3:
            r = p, g = q, b = v;
            break;
        case 4:
            r = t, g = p, b = v;
            break;
        case 5:
            r = v, g = p, b = q;
            break;
            //Just in case
        case 6:
            r = v, g = t, b = p;
            break;
        case -1:
            r = v, g = p, b = q;
            break;
    }

    Color rgba = {0, 0, 0, 0};

    rgba.r = r * 255;
    rgba.g = g * 255;
    rgba.b = b * 255;
    rgba.a = alpha;

    return rgba;
}
//Save screenshot with time-stamp
void TakeTimeStampedScreenshot() {
    //Create file name buffer
    char curent_time[200];
    //Pull time stamp
    double time_stamp = GetTime();
    //Coppy time stamp into character buffer with one milisecond precision
    sprintf(curent_time, "%.3lf.png", time_stamp);
    //Take screenshot
    TakeScreenshot((const char *) curent_time);
}

//Vectro3 swapper
void swap_vectors3 (Vector3 *a, Vector3 *b){    
    Vector3 temp = *a;
    *a = *b;
    *b = temp;
}
// HSL format bouble sort
void HSLColorsSort(Vector3 hsl_colors[], int n, enum sort_mode mode) {
    
    int i, j;
    
    for (i = 0; i < n - 1; i++)
    {
        for (j = 0; j < (n - i - 1); j++)
        {
            switch (mode){
                case HUE:{
                    if (hsl_colors[j].x < hsl_colors[j + 1].x){
                        swap_vectors3(&hsl_colors[j],&hsl_colors[j + 1]);
                    }
                    break;
                }
                case SATURATION:{
                    if (hsl_colors[j].y < hsl_colors[j + 1].y){
                        swap_vectors3(&hsl_colors[j],&hsl_colors[j + 1]);
                    }
                    break;
                }
                case BRIGHTNES:{
                    if (hsl_colors[j].z < hsl_colors[j + 1].z){
                        swap_vectors3(&hsl_colors[j],&hsl_colors[j + 1]);
                    }
                    break;
                }
            }
             
        }
    }
}

// Direct memory coppy version
void ColorPicker(Image image, Color *image_buf, Color *back_color, int x, int y) {

    unsigned int id = (image.width * y) + x;
    memcpy(back_color, &image_buf[id], sizeof (Color));
}

int max(int a, int b) {
    return a > b ? a : b;
}
//Alternative hue comperation function needed for standard qsot 
int hsl_hue_compare(const void *a, const void *b){
    Vector3 *a_buf = (Vector3 *)a;
    Vector3 *b_buf = (Vector3 *)b;
    
    if (a_buf->x < b_buf->x)
      return -1;
   else if (a_buf->x > b_buf->x)
      return 1;
   else
      return 0;
}



