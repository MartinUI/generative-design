#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include "raylib.h"
#include "raymath.h"

Color ColorFromHSB(Vector3);
void TakeTimeStampedScreenshot();
Vector3 HSVToHSB(Vector3 hsv);
Vector3 HSBToHSV(Vector3 hsb);

int main(int argc, char *argv[]){
	// Initialization 
	int screen_width = 680;
	int screen_height = 600;

	int mouse_x, mouse_y;

	int tile_count_x = 2;
	int tile_count_y = 10;	

	Vector3 colors_left[tile_count_y];
	Vector3 colors_right[tile_count_y];

	bool interpolate_shortest = true;

  	for (int i = 0; i < tile_count_y; i++) {
		Vector3 colors_left_buffor;
		colors_left_buffor.x = GetRandomValue(0,60);
		colors_left_buffor.y = GetRandomValue(0,100);
		colors_left_buffor.z = 100.0;
		colors_left[i] = HSBToHSV(colors_left_buffor);

		Vector3 colors_right_buffor;
  		colors_right_buffor.x = GetRandomValue(160,190);
		colors_right_buffor.y = 100.0;
		colors_right_buffor.z = GetRandomValue(0, 100);
		colors_right[i] = HSBToHSV(colors_right_buffor);
	}

	for (int i = 0; i < tile_count_y; i++) {
		

	}

	InitWindow(screen_width, screen_height, "P_1_1_1_02");

	SetTargetFPS(60);

	// Main game loop
	while (!WindowShouldClose()) // Detect window close button or ESC key
	{

		if (IsKeyDown(KEY_ONE)) interpolate_shortest = true; 
		else if (IsKeyDown(KEY_TWO)) interpolate_shortest = false;
		else if (IsKeyDown(KEY_S)) TakeTimeStampedScreenshot();

		mouse_x = Clamp(GetMouseX(),0, screen_width);
		mouse_y = Clamp(GetMouseY(),0, screen_height);

		float tile_width = (int)((float)screen_width / (float)tile_count_x);
		float tile_height = (int)((float)screen_height / (float)tile_count_y);

		//tile_count_x = (int)map(mouse_x, 0, screen_width, 2, 100);
		tile_count_x = (int)Lerp(2, 100, (float)mouse_x / (float)screen_width);
		tile_count_y = (int)Lerp(2, 10, (float)mouse_y / (float)screen_height);
		//tile_count_y = (int)map(mouse_y, 0, screen_height, 2, 10);
		Color inter_color;

		// Draw
		BeginDrawing();
		ClearBackground(RAYWHITE);

		for (int grid_y = 0; grid_y < tile_count_y; grid_y++)
		{
			Color col1 = ColorFromHSV(colors_left[grid_y]);
			Color col2 = ColorFromHSV(colors_right[grid_y]);
			//Color col1 = ColorFromHSV(colors_left[grid_y]);
			//Color col2 = ColorFromHSV(colors_right[grid_y]);

			for (int grid_x = 0; grid_x < tile_count_x; grid_x++)
			{
				float amount = Lerp(0.0, 1.0, (float)grid_x / (float)tile_count_x); //map(grid_x, 0, tile_count_x - 1, 0, 1);

				if (interpolate_shortest)
				{
					int r = (int)Lerp(col1.r, col2.r, amount); // lerp(col1.r,col2.r,amount);
					int g = (int)Lerp(col1.g, col2.g, amount); //lerp(col1.g,col2.g,amount);
					int b = (int)Lerp(col1.b, col2.b, amount); // lerp(col1.b,col2.b,amount);

					inter_color = (Color){r,g,b,255};
				}
				else
				{
					Vector3 left_hsb = HSVToHSB(colors_left[grid_x]);
					Vector3 right_hsb = HSVToHSB(colors_right[grid_y]); 
					int x = (int)Lerp(left_hsb.x, right_hsb.x, amount);
					int y = (int)Lerp(left_hsb.y, right_hsb.y, amount);
					int z = (int)Lerp(left_hsb.z, right_hsb.z, amount);
					Vector3 hsb = {x,y,z};
					inter_color = ColorFromHSB(hsb);
				}
				//fill(inter_col);
				float pos_x = tile_width * (int)grid_x;
				float pos_y = tile_height * (int)grid_y;
				//DrawRectangle(pos_x, pos_y, tile_width, tile_height,inter_color);
				DrawRectangleV((Vector2){pos_x, pos_y}, (Vector2){tile_width, tile_height},inter_color);
				// just for ase export
			}
		}
		EndDrawing();
	}
	// De-Initialization
	CloseWindow(); // Close window and OpenGL context
	return 0;
}

//Hue 0.0-360.0 float Saturation 0.0-100.0, Level 0.0-100.0, alpha 0-255
Color ColorFromHSB(Vector3 hsv)
{
	float r, g, b;
	r = g = b = 0.0;

	float h = hsv.x / 360;
	float s = hsv.y / 100;
	float v = hsv.z / 100;

	int i = floor(h * 6);
	float f = h * 6 - i;
	float p = v * (1 - s);
	float q = v * (1 - f * s);
	float t = v * (1 - (1 - f) * s);

	switch (i % 6)
	{
	case 0:
		r = v, g = t, b = p;
		break;
	case 1:
		r = q, g = v, b = p;
		break;
	case 2:
		r = p, g = v, b = t;
		break;
	case 3:
		r = p, g = q, b = v;
		break;
	case 4:
		r = t, g = p, b = v;
		break;
	case 5:
		r = v, g = p, b = q;
		break;
	//Just in case
	case 6:
		r = v, g = t, b = p;
		break;
	case -1:
		r = v, g = p, b = q;
		break;
	}

	Color rgba = {0, 0, 0, 0};

	rgba.r = r * 255;
	rgba.g = g * 255;
	rgba.b = b * 255;
	rgba.a = 255;

	return rgba;
}

void TakeTimeStampedScreenshot()
{
	//Create file name buffer
	char curent_time[256];
	//Pull time stamp
	double time_stamp = GetTime();
	//Coppy time stamp into character buffer with one milisecond precision
	sprintf(curent_time, "%.3lf.png", time_stamp);
	//Take screenshot
	TakeScreenshot((const char *)curent_time);
}

Vector3 HSVToHSB(Vector3 hsv){
	Vector3 hsb;
	hsb.x = hsv.x;
	hsb.z = (2.0 - hsv.y) * hsv.z;
 	hsb.y = hsv.y * hsv.z;
	hsb.y /= (hsb.z <= 1.0) ? hsb.z : 2.0 - hsb.z;
 	hsb.z /= 2.0;
	return hsb;
}

Vector3 HSBToHSV(Vector3 hsb){
	Vector3 hsv;
	hsv.x = hsb.x;
	hsb.z *= 2.0;
	hsb.y *= (hsb.z <= 1.0) ? hsb.z : 2.0 - hsb.z;
	hsv.z = (hsb.z + hsb.y) / 2.0;
	hsv.y = (2.0 * hsb.y) / (hsb.z + hsb.y);
	return hsv;
}
