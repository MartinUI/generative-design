#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "raylib.h"

Color ColorFromHSB(Vector3);
Vector3 HSVToHSB(Vector3 hsv);
Vector3 HSBToHSV(Vector3 hsb);

int main() {

    int screenWidth = 720;
    int screenHeight = 720;

    InitWindow(screenWidth, screenHeight, "P_1_0_01");

    SetTargetFPS(60);

    // Main game loop
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        // Update
        Color test = {23, 18, 200, 255};
        Vector3 hsv_test = ColorToHSV(test);
        Color hsv_back_test = ColorFromHSV (hsv_test);
        Vector3 hsb_test = HSVToHSB (hsv_test);
        Color hsb_back_test = ColorFromHSB (hsb_test);
        Vector3 full_hsv_test = HSBToHSV (hsb_test);
        Color test_back = ColorFromHSV(full_hsv_test);


        BeginDrawing();
        ClearBackground(WHITE);
        DrawRectangle(0, 0, screenWidth / 4, screenHeight, test);
        DrawRectangle(screenWidth / 4, 0, screenWidth / 3, screenHeight, hsv_back_test);
        DrawRectangle(screenWidth / 2, 0, screenWidth / 3, screenHeight, hsb_back_test);
        DrawRectangle(screenWidth / 4 * 3, 0, screenWidth / 3, screenHeight, test_back);
        EndDrawing();
    }
    CloseWindow();
    return 0;
}

//Hue 0.0-360.0 float Saturation 0.0-100.0, Level 0.0-100.0
Color ColorFromHSB(Vector3 hsv)
{
	float r, g, b;
	r = g = b = 0.0;

	float h = hsv.x / 360;
	float s = hsv.y / 100;
	float v = hsv.z / 100;

	int i = floor(h * 6);
	float f = h * 6 - i;
	float p = v * (1 - s);
	float q = v * (1 - f * s);
	float t = v * (1 - (1 - f) * s);

	switch (i % 6)
	{
	case 0:
		r = v, g = t, b = p;
		break;
	case 1:
		r = q, g = v, b = p;
		break;
	case 2:
		r = p, g = v, b = t;
		break;
	case 3:
		r = p, g = q, b = v;
		break;
	case 4:
		r = t, g = p, b = v;
		break;
	case 5:
		r = v, g = p, b = q;
		break;
	//Just in case
	case 6:
		r = v, g = t, b = p;
		break;
	case -1:
		r = v, g = p, b = q;
		break;
	}

	Color rgba = {0, 0, 0, 0};

	rgba.r = r * 255;
	rgba.g = g * 255;
	rgba.b = b * 255;
	rgba.a = 255;

	return rgba;
}

Vector3 HSVToHSB(Vector3 hsv){
	Vector3 hsb;
	hsb.x = hsv.x;
	hsb.z = (2.0 - hsv.y) * hsv.z;
 	hsb.y = hsv.y * hsv.z;
	hsb.y /= (hsb.z <= 1.0) ? hsb.z : 2.0 - hsb.z;
 	hsb.z /= 2.0;

	return hsb;
}

Vector3 HSBToHSV(Vector3 hsb){
	Vector3 hsv;
	hsv.x = hsb.x;
	hsb.z *= 2.0;
	hsb.y *= (hsb.z <= 1.0) ? hsb.z : 2.0 - hsb.z;
	hsv.z = (hsb.z + hsb.y) / 2.0;
	hsv.y = (2.0 * hsb.y) / (hsb.z + hsb.y);

	return hsv;
}
