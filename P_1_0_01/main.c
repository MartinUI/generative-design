#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "raylib.h"

Color HSVToColor(Vector3, int);
void TakeTimeStampedScreenshot();

int main() {

    int screenWidth = 720;
    int screenHeight = 720;
    
    InitWindow(screenWidth, screenHeight, "P_1_0_01");

    SetTargetFPS(60); 

    // Main game loop
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        // Update

        if (IsKeyDown(KEY_S)) TakeTimeStampedScreenshot();

        int mouse_x = GetMouseX();
        int mouse_y = GetMouseY();

		
        Vector3 hsv_color = {(float)mouse_y / 2.0, 1.0,1.0};
		Color bg_color = ColorFromHSV(hsv_color);

        Vector3 hsv_rect_color = {360.0 - (float)mouse_y / 2, 1.0, 1.0};
        Color rect_color = ColorFromHSV(hsv_rect_color);
        
        BeginDrawing();


        ClearBackground(bg_color);

        int rectangle_x = screenWidth / 2 - (mouse_x + 1) / 2;
        int rectangle_y = screenHeight / 2 - (mouse_x + 1) / 2;

        DrawRectangle(rectangle_x, rectangle_y, mouse_x + 1, mouse_x + 1, rect_color);

        EndDrawing();
    }
    CloseWindow();
    return 0;
}

void TakeTimeStampedScreenshot() {
    //Create file name buffer
    char curent_time[256];
    //Pull time stamp
    double time_stamp = GetTime();
    //Coppy time stamp into character buffer with one milisecond precision
    sprintf(curent_time, "%.3lf.png", time_stamp);
    //Take screenshot
    TakeScreenshot((const char *) curent_time);
}
