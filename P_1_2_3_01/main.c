#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include "raylib.h"

Color HSVToColor(Vector3, int);
void TakeTimeStampedScreenshot();
int map(int, int, int, int, int);
int clamp(int, int);

int main(int argc, char *argv[]) {
    // Initialization
    //--------------------------------------------------------------------------------------
    int screen_width = 800;
    int screen_height = 800;


    int tiel_count_x = 50;
    int tiel_count_y = 10;

    float hue_values[tiel_count_x];
    float saturation_values[tiel_count_x];
    float brightness_values[tiel_count_x];

    for (int i = 0; i < tiel_count_x; i++) {
        hue_values[i] = (float)GetRandomValue(0, 360);
        saturation_values[i] = (float)GetRandomValue(0, 100)/100;
        brightness_values[i] = (float)GetRandomValue(0, 100)/100;
    }
    
    //printf("%f %f %f\n",hue_values[0],saturation_values[0],brightness_values[0]);
    
    Vector3 hsv_color;
    Color rgba_color;


    InitWindow(screen_width, screen_height, "P_1_2_3_01");

    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        //oryginal book example 0-9 replacement withQ-P
        if (IsKeyDown(KEY_S)) TakeTimeStampedScreenshot();
        else if(IsKeyReleased(KEY_Q)){
            for(int i = 0; i < tiel_count_x; i++) {
                hue_values[i] = (float)GetRandomValue(0, 360);
                saturation_values[i] = (float)GetRandomValue(0, 100)/100;
                brightness_values[i] = (float)GetRandomValue(0, 100)/100;
            }
        }else if(IsKeyReleased(KEY_W)){
            for(int i = 0; i < tiel_count_x; i++) {
                hue_values[i] = (float)GetRandomValue(0, 360);
                saturation_values[i] = (float)GetRandomValue(0, 100)/100;
                brightness_values[i] = 1.0;
            }
        }else if(IsKeyReleased(KEY_E)){
            for(int i = 0; i < tiel_count_x; i++) {
                hue_values[i] = (float)GetRandomValue(0, 360);
                saturation_values[i] = 1.0;
                brightness_values[i] = (float)GetRandomValue(0, 100)/100;
            }
        }else if(IsKeyReleased(KEY_R)){
            for(int i = 0; i < tiel_count_x; i++) {
                hue_values[i] = 0.0;
                saturation_values[i] = 0.0;
                brightness_values[i] = (float)GetRandomValue(0, 100)/100;
            }
        }else if(IsKeyReleased(KEY_T)){
            for(int i = 0; i < tiel_count_x; i++) {
                hue_values[i] = 195.0;
                saturation_values[i] = 1.0;
                brightness_values[i] = (float)GetRandomValue(0, 100)/100;
            }
        }else if(IsKeyReleased(KEY_Y)){
            for(int i = 0; i < tiel_count_x; i++) {
                hue_values[i] = 195.0;
                saturation_values[i] = (float)GetRandomValue(0, 100)/100;
                brightness_values[i] = 1.0;
            }
        }else if(IsKeyReleased(KEY_U)){
            for(int i = 0; i < tiel_count_x; i++) {
                hue_values[i] = (float)GetRandomValue(0, 180);
                saturation_values[i] = (float)GetRandomValue(80, 100)/100;
                brightness_values[i] = (float)GetRandomValue(50, 90)/100;
            }
        }else if(IsKeyReleased(KEY_I)){
            for(int i = 0; i < tiel_count_x; i++) {
                hue_values[i] = (float)GetRandomValue(180, 360);
                saturation_values[i] = (float)GetRandomValue(80, 100)/100;
                brightness_values[i] = (float)GetRandomValue(50, 90)/100;
            }
        }else if(IsKeyReleased(KEY_O)){
            for(int i = 0; i < tiel_count_x; i++) {
                if (i % 2 == 0) {
                    hue_values[i] = (float)GetRandomValue(0, 360);
                    saturation_values[i] = 1.0;
                    brightness_values[i] = (float)GetRandomValue(0, 100)/100;
                }else{
                    hue_values[i] = 195.0;
                    saturation_values[i] = (float)GetRandomValue(0, 100)/100;
                    brightness_values[i] = 1.0;
                }
            }
        }else if(IsKeyReleased(KEY_P)){
            for(int i = 0; i < tiel_count_x; i++) {
                if (i % 2 == 0) {
                    hue_values[i] = 192.0;
                    saturation_values[i] = (float)GetRandomValue(0, 100)/100;
                    brightness_values[i] = (float)GetRandomValue(10, 100)/100;
                }else{
                    hue_values[i] = 273.0;
                    saturation_values[i] = (float)GetRandomValue(0, 100)/100;
                    brightness_values[i] = (float)GetRandomValue(10, 90)/100;
                }
            }
        }

        int mouse_x = clamp(GetMouseX(), screen_height);
        int mouse_y = clamp(GetMouseY(), screen_width);

        // count every tile
        int counter = 0;

        // map mouse to grid resolution
        int current_tile_count_x = (int) map(mouse_x, 0, screen_width, 1, tiel_count_x);
        int current_tile_count_y = (int) map(mouse_y, 0, screen_height, 1, tiel_count_y);
        float tile_width = screen_width / (float) current_tile_count_x;
        float tile_height = screen_height / (float) current_tile_count_y;

        // Draw
        BeginDrawing();

        for (int grid_y = 0; grid_y < tiel_count_y; grid_y++) {
            for (int grid_x = 0; grid_x < tiel_count_x; grid_x++) {
                float posX = tile_width*grid_x;
                float posY = tile_height*grid_y;
                int index = counter % current_tile_count_x;
                // get component color values                
                hsv_color = (Vector3){hue_values[index],saturation_values[index],brightness_values[index]};                
                rgba_color = HSVToColor(hsv_color,255);
                //Add +1 to width and height for better screen coverage     
                DrawRectangle(posX, posY, tile_width + 1, tile_height + 1, rgba_color);
                counter++;
            }
        }

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow(); // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}

//Hue 0.0-360.0 float Saturation 0.0-1.0, Level 0.0-1.0, alpha 0-255

Color HSVToColor(Vector3 hsv, int alpha) {
    float r, g, b;
    r = g = b = 0.0;

    float h = hsv.x / 360;
    float s = hsv.y;
    float v = hsv.z;

    int i = floor(h * 6);
    float f = h * 6 - i;
    float p = v * (1 - s);
    float q = v * (1 - f * s);
    float t = v * (1 - (1 - f) * s);

    switch (i % 6) {
        case 0:
            r = v, g = t, b = p;
            break;
        case 1:
            r = q, g = v, b = p;
            break;
        case 2:
            r = p, g = v, b = t;
            break;
        case 3:
            r = p, g = q, b = v;
            break;
        case 4:
            r = t, g = p, b = v;
            break;
        case 5:
            r = v, g = p, b = q;
            break;
            //Just in case
        case 6:
            r = v, g = t, b = p;
            break;
        case -1:
            r = v, g = p, b = q;
            break;
    }


    Color rgba = {0, 0, 0, 0};

    rgba.r = r * 255;
    rgba.g = g * 255;
    rgba.b = b * 255;
    rgba.a = alpha;

    return rgba;
}

void TakeTimeStampedScreenshot() {
    //Create file name buffer
    char curent_time[200];
    //Pull time stamp
    double time_stamp = GetTime();
    //Coppy time stamp into character buffer with one milisecond precision
    sprintf(curent_time, "%.3lf.png", time_stamp);
    //Take screenshot
    TakeScreenshot((const char *) curent_time);
}
//simple int value mapper

int map(int x, int in_min, int in_max, int out_min, int out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
//simple int clamp to l limit

int clamp(int i, int l) {
    if (i < 0)
        return 0;
    if (i > l)
        return l;
    return i;
}
